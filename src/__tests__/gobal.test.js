const text = 'Hola Mundo';
const fruits = ['Manzana', 'Platano']
const mi_edad = 21
const ella_me_ama = false

test('Debe contener un texto', () => {
    expect(text).toMatch(/Mundo/);
})

test('¿Hay manzana?', () => {
    expect(fruits).toContain('Manzana');
})

test('Mayor que', () => {
    expect(mi_edad).toBeGreaterThan(18);
})

test('Ella no me ama', () => {
    expect(ella_me_ama).toBeFalsy()
})

// PROBANDO PROMESAS
const reverseString = str => {
    return new Promise((resolve, reject) => {
        if (!str) {
            reject(Error('Error'))
        }
        resolve(str.split('').reverse().join(''))
    })
};

test('Probando promesa', () => {
    return reverseString('Hola')
    .then(string => {
        expect(string).toBe('aloH')
    });
})

// PROBANDO ASYNC/AWAIT
test('Probando async/awiat', async () =>{
    const string = await reverseString('Hola');
    expect(string).toBe('aloH');
})

// afterEach(() => console.log('Despues de cada prueba'))
// afterAll(() => console.log('Despues que todos'))

// beforeEach(() => console.log('Antes de cada prueba'))
// beforeAll(() => console.log('Antes que todos'))
