const randomStrings = require('../index');

describe('Probando funcionalidades de RandomStrings', () => {
    test('Probar funcionalidad', () => {
        expect(typeof (randomStrings())).toBe('string');
    });

    test('Comprobar que no exite una ciudad', () => {
        expect(randomStrings()).not.toMatch(/Cusco/);
    })
});